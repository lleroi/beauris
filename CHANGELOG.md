# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Breaking changes

- `deploy_perms` service was renamed `deploy_authelia`, need to be updaed in `beauris.yml` in `tasks` section
- `interface_setup` local workflow was renamed `deploy_all`, need to be updated in `gitlab-ci.yml`
- `# __GOGEPP3000_RULES__` placeholder in authelia conf file was renamed to `# __BEAURIS_RULES__`
- In `beauris.yml`, in `data_locker`>`options`>`pattern_derived`, user `{name}` instead of `{task_id}`
- In `beauris.yml`, under `deploy`>`servers`>`staging|production`, new `base_url_restricted` and `url_prefix_restricted` are required
- *jbrowse_data_dirs* variable is replaced by *extra_ref_data_dirs*, both in beauris.yml and ansible templates.

### Added

- ORSON: add descriptions to fasta and gff files (!19, !20, thanks @Kamassau)
- Manage mix of private and public data
- Add script to cleanup work dir
- Expression data handling (finished)
- Add links from blast results to genoboo
- Add links from JBrowse/Apollo to genoboo
- Add support for extra files at organism and assembly level
- Improved Elasticsearch integration
- Add management of the 'logging-debug' MR label to set logger to debug
- Add basic management of VCF files (TBA: testing)
- Cleanup old_data_dir if it exists (it shouldn't) when deploying jbrowse/genoboo/ES
- Bot-ris will now send validation error to MR
- Bot-ris will not send interface url to MR after deploy
- Better check for files in yaml (will check container folders to detect mount / permissions issues) 
- Support xrefs at assembly and annotation level

### Changed

- Refactored docker deployment code
- Fixed unwanted replacement of transcript suffix in transcript/CDS outputs of gffread
- Fixed tool_version not updated in locked yml when a tool is rerun in a new version
- Changed the way we delete files when deploying interfaces. They can now be deleted after updating. For now, active in genoboo only.
- Do not generate Genoboo, Elasticsearch or JBrowse datasets when the crresponding services are disabled
- Will raise an error in OGS check if any UTR, CDS or exon is not contained by the parent mRNA, unless the --extend-parent option is passed, in which case the mRNA position will be extended to match
- Make common_name optional, and make use of it when provided

### Fixed

- Fixed 'run_on_touched_orgs', which was detecting changes 'both ways'
- Fixed service clause
- Fixed purge() methodes for tracks and additional files
- Fixed various issues with mixed_perms deployments

## [0.2] - 2023-11-10

### Added

- Documentation on [https://beauris.readthedocs.io](https://beauris.readthedocs.io/)
- Genoboo integration
- Make Ansible templates overridable (#49)
- Expression data handling (started)
- Implement organism pictures
- Elasticsearch integration
- jbrowse_data_dirs variable in beauris.yml for additional folders to be mounted in the jbrowse container.

### Changed

- Fixed Orson execution (!3)
- Reworked Apollo permissions update (!1)
- Fix interfaces bugs (#51 and #43)
- Fix execution of DRMAA tasks
- Add support for DNASeq bams
- Change bam_to_wig method (faster, uses less memory)
- Fixed unusable date in entities metadata

## [0.1] - 2023-03-31

### Added

- First ever release of Beauris
