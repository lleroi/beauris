import os

from . import OgsCheckTestCase


class TestOgsCheck(OgsCheckTestCase):

    def test_work_dir(self, ogs_check, temp_dir):

        test_data_dir = "test-data/data/"

        out_gff = os.path.join(temp_dir, 'out.gff')
        assert not ogs_check.check(test_data_dir + "test.gff", test_data_dir + "genome.fa", out_gff)

        expected = test_data_dir + "expected.gff"

        with open(expected, 'r') as exp:
            with open(out_gff, 'r') as gff:
                assert exp.read() == gff.read()

    def test_rna_prefix(self, ogs_check_rna_prefix, temp_dir):

        test_data_dir = "test-data/data/"

        out_gff = os.path.join(temp_dir, "out_rna_prefix.gff")
        assert not ogs_check_rna_prefix.check(test_data_dir + "test_rna_prefix.gff", test_data_dir + "genome.fa", out_gff)

        expected = test_data_dir + "expected_rna_prefix.gff"

        with open(expected, "r") as exp:
            with open(out_gff, "r") as out:
                assert exp.read() == out.read()

    def test_misplaced_extend(self, ogs_check_extend_parent, temp_dir):
        test_data_dir = "test-data/data/"

        out_gff = os.path.join(temp_dir, "out_extended.gff")
        assert not ogs_check_extend_parent.check(test_data_dir + "test_misplaced.gff", test_data_dir + "genome.fa", out_gff)

        expected = test_data_dir + "expected_extended.gff"

        with open(expected, "r") as exp:
            with open(out_gff, "r") as out:
                assert exp.read() == out.read()

    def test_misplaced_raise(self, ogs_check, temp_dir):
        test_data_dir = "test-data/data/"

        out_gff = os.path.join(temp_dir, "out_misplaced.gff")
        assert ogs_check.check(test_data_dir + "test_misplaced.gff", test_data_dir + "genome.fa", out_gff)

        expected = test_data_dir + "expected_misplaced.gff"

        with open(expected, "r") as exp:
            with open(out_gff, "r") as out:
                assert exp.read() == out.read()
