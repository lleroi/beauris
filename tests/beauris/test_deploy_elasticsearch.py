import os

import yaml

from . import BeaurisTestCase


class TestDeployElasticsearch(BeaurisTestCase):

    def test_interface_data_dir(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_elasticsearch")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('elasticsearch', server, org)

        assert deployer._get_data_dir() == os.path.join(deploy_dir, 'Blabla/Blobloblo/docker_data/elasticsearch')
        assert deployer._get_data_dir("elasticsearch") == os.path.join(deploy_dir, 'Blabla/Blobloblo/elasticsearch')

    def test_restricted_org_elasticsearch(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        org.derived_files['build_elasticsearch'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/es.tar.bz2'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_elasticsearch")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('elasticsearch', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir, "data"))

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "elasticsearch:" in doc
            assert "elasticsearch-query-restricted:" not in doc

    def test_restricted_ass_elasticsearch(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        org.derived_files['build_elasticsearch'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/es.tar.bz2'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_elasticsearch")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('elasticsearch', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir, "data"))

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert org.has_mixed_data()
        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = yaml.safe_load(docompf)

            assert "elasticsearch" in doc['services']
            assert "elasticsearch-query" in doc['services']

            assert doc['services']['elasticsearch']['volumes'] == [
                "./docker_data/elasticsearch/data:/usr/share/elasticsearch/data",
            ]

            assert doc['services']['elasticsearch-query']['environment']['RESTRICT_PUBLIC'] == "True"

            assert doc['services']['elasticsearch-query']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-elasticsearch-query.rule=(Host(`staging.example.org`) && PathPrefix(`/sp/blabla_blobloblo/query`))",
                "traefik.http.routers.blabla_blobloblo_staging-elasticsearch-query.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-elasticsearch-query.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-elasticsearch-query.middlewares=sp-auth,sp-app-prefix",
                "traefik.http.services.blabla_blobloblo_staging-elasticsearch-query.loadbalancer.server.port=80",
            ]

            assert "elasticsearch-restricted" not in doc['services']
            assert "elasticsearch-query-restricted" in doc['services']

            assert doc['services']['elasticsearch-query-restricted']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-elasticsearch-query-restricted.rule=(Host(`restricted.staging.example.org`) && PathPrefix(`/sp_restricted/blabla_blobloblo/query`))",
                "traefik.http.routers.blabla_blobloblo_staging-elasticsearch-query-restricted.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-elasticsearch-query-restricted.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-elasticsearch-query-restricted.middlewares=sp-auth,sp-app-prefix",
                "traefik.http.services.blabla_blobloblo_staging-elasticsearch-query-restricted.loadbalancer.server.port=80",
            ]

    def test_restricted_ann_elasticsearch(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        org.derived_files['build_elasticsearch'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/es.tar.bz2'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_elasticsearch")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('elasticsearch', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir, "data"))

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "elasticsearch:" in doc
            assert "elasticsearch-query-restricted:" in doc

    def test_unrestricted_org_elasticsearch(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        org.derived_files['build_elasticsearch'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/es.tar.bz2'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_elasticsearch")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('elasticsearch', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir, "data"))

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "elasticsearch:" in doc
            assert "elasticsearch-query-restricted:" not in doc
