import os

import yaml

from . import BeaurisTestCase


class TestDeployGenomehomepage(BeaurisTestCase):

    def test_interface_data_dir(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genomehomepage")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genomehomepage', server, org)

        assert deployer._get_data_dir() == os.path.join(deploy_dir, 'Blabla/Blobloblo/docker_data/genomehomepage')
        assert deployer._get_data_dir("genomehomepage") == os.path.join(deploy_dir, 'Blabla/Blobloblo/genomehomepage')

    def test_restricted_org_genomehomepage(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genomehomepage")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genomehomepage', server, org)
        out_dir = deployer._get_data_dir("site")
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir, "index.html"))
        # No assets with default template
        # assert os.path.isdir(os.path.join(out_dir, "assets"))

        out_dir_restricted = deployer._get_data_dir("site", restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "proxy:" in doc
            assert "proxy-restricted:" not in doc

    def test_restricted_ass_genomehomepage(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genomehomepage")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genomehomepage', server, org)
        out_dir = deployer._get_data_dir("site")
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir, "index.html"))

        out_dir_restricted = deployer._get_data_dir("site", restricted=True)

        assert org.has_mixed_data()
        assert os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = yaml.safe_load(docompf)

            assert "proxy" in doc['services']

            assert "./site/:/usr/local/nginx/html/:ro" in doc['services']['proxy']['volumes']
            assert doc['services']['proxy']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-nginx.rule=(Host(`staging.example.org`) && PathPrefix(`/sp/blabla_blobloblo`))",
                "traefik.http.routers.blabla_blobloblo_staging-nginx.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-nginx.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-nginx.middlewares=sp-auth,sp-trailslash,sp-prefix",
                "traefik.http.services.blabla_blobloblo_staging-nginx.loadbalancer.server.port=80",
            ]

            assert "proxy-restricted" in doc['services']

            assert "./site_restricted/:/usr/local/nginx/html/:ro" in doc['services']['proxy-restricted']['volumes']
            assert doc['services']['proxy-restricted']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-nginx-restricted.rule=(Host(`restricted.staging.example.org`) && PathPrefix(`/sp_restricted/blabla_blobloblo`))",
                "traefik.http.routers.blabla_blobloblo_staging-nginx-restricted.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-nginx-restricted.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-nginx-restricted.middlewares=sp-auth,sp-trailslash,sp-prefix",
                "traefik.http.services.blabla_blobloblo_staging-nginx-restricted.loadbalancer.server.port=80",
            ]

    def test_restricted_ann_genomehomepage(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genomehomepage")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genomehomepage', server, org)
        out_dir = deployer._get_data_dir("site")
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir, "index.html"))

        out_dir_restricted = deployer._get_data_dir("site", restricted=True)

        assert os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "proxy:" in doc
            assert "proxy-restricted:" in doc

    def test_unrestricted_org_genomehomepage(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genomehomepage")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genomehomepage', server, org)
        out_dir = deployer._get_data_dir("site")
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir, "index.html"))

        out_dir_restricted = deployer._get_data_dir("site", restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "proxy:" in doc
            assert "proxy-restricted:" not in doc
