import pytest

from . import BeaurisTestCase


class TestEntityRestricted(BeaurisTestCase):

    def test_restricted_org(self, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        assert org.restricted_to == "some_group"
        assert org.assemblies[0].restricted_to == "some_group"
        assert org.assemblies[0].annotations[0].restricted_to == "some_group"

        assert org.get_metadata()['restricted_to'] == "some_group"
        assert org.assemblies[0].get_metadata()['restricted_to'] == "some_group"
        assert org.assemblies[0].annotations[0].get_metadata()['restricted_to'] == "some_group"

        assert not org.has_public_data()
        assert org.has_restricted_data()
        assert not org.has_mixed_data()
        assert org.get_restricted_tos() == ['some_group']
        assert org.get_restricted_to_map() == {'blabla_blobloblo': 'some_group'}

        # Now try purging
        purged_org = org.copy_and_purge_restricted_data()

        assert purged_org is None

        # Did purging altered the original org?
        assert org.restricted_to == "some_group"
        assert org.assemblies[0].restricted_to == "some_group"
        assert org.assemblies[0].annotations[0].restricted_to == "some_group"

        assert org.get_metadata()['restricted_to'] == "some_group"
        assert org.assemblies[0].get_metadata()['restricted_to'] == "some_group"
        assert org.assemblies[0].annotations[0].get_metadata()['restricted_to'] == "some_group"

        assert not org.has_public_data()
        assert org.has_restricted_data()
        assert not org.has_mixed_data()
        assert org.get_restricted_tos() == ['some_group']
        assert org.get_restricted_to_map() == {'blabla_blobloblo': 'some_group'}

    def test_unrestricted_org(self, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        assert org.restricted_to is None
        assert org.assemblies[0].restricted_to is None
        assert org.assemblies[0].annotations[0].restricted_to is None

        assert org.get_metadata()['restricted_to'] is None
        assert org.assemblies[0].get_metadata()['restricted_to'] is None
        assert org.assemblies[0].annotations[0].get_metadata()['restricted_to'] is None

        assert org.has_public_data()
        assert not org.has_restricted_data()
        assert not org.has_mixed_data()
        assert org.get_restricted_tos() == []
        assert org.get_restricted_to_map() == {}

        # Now try purging
        purged_org = org.copy_and_purge_restricted_data()

        assert purged_org is not None
        assert purged_org.restricted_to is None
        assert purged_org.assemblies[0].restricted_to is None
        assert purged_org.assemblies[0].annotations[0].restricted_to is None

        assert purged_org.get_metadata()['restricted_to'] is None
        assert purged_org.assemblies[0].get_metadata()['restricted_to'] is None
        assert purged_org.assemblies[0].annotations[0].get_metadata()['restricted_to'] is None

        assert purged_org.has_public_data()
        assert not purged_org.has_restricted_data()
        assert not org.has_mixed_data()
        assert org.get_restricted_tos() == []
        assert org.get_restricted_to_map() == {}

        # Did purging altered the original org?
        assert org.restricted_to is None
        assert org.assemblies[0].restricted_to is None
        assert org.assemblies[0].annotations[0].restricted_to is None

        assert org.get_metadata()['restricted_to'] is None
        assert org.assemblies[0].get_metadata()['restricted_to'] is None
        assert org.assemblies[0].annotations[0].get_metadata()['restricted_to'] is None

        assert org.has_public_data()
        assert not org.has_restricted_data()
        assert not org.has_mixed_data()
        assert org.get_restricted_tos() == []
        assert org.get_restricted_to_map() == {}

    def test_restricted_ass(self, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        assert org.restricted_to is None
        assert org.assemblies[0].restricted_to == "some_group"
        assert org.assemblies[0].annotations[0].restricted_to == "some_group"
        assert org.assemblies[1].restricted_to is None
        assert org.assemblies[1].annotations[0].restricted_to is None

        assert org.get_metadata()['restricted_to'] is None
        assert org.assemblies[0].get_metadata()['restricted_to'] == "some_group"
        assert org.assemblies[0].annotations[0].get_metadata()['restricted_to'] == "some_group"
        assert org.assemblies[1].get_metadata()['restricted_to'] is None
        assert org.assemblies[1].annotations[0].get_metadata()['restricted_to'] is None

        assert org.has_public_data()
        assert org.has_restricted_data()
        assert org.has_mixed_data()
        assert org.get_restricted_tos() == ["some_group"]
        assert org.get_restricted_to_map() == {'blabla_blobloblo/assembly_10': 'some_group'}
        assert not org.assemblies[0].has_public_data()
        assert org.assemblies[0].has_restricted_data()
        assert not org.assemblies[0].has_mixed_data()
        assert org.assemblies[0].get_restricted_tos() == ["some_group"]
        assert org.assemblies[0].get_restricted_to_map() == {'blabla_blobloblo/assembly_10': 'some_group'}
        assert org.assemblies[1].has_public_data()
        assert not org.assemblies[1].has_restricted_data()
        assert not org.assemblies[1].has_mixed_data()
        assert org.assemblies[1].get_restricted_tos() == []
        assert org.assemblies[1].get_restricted_to_map() == {}

        # Now try purging
        purged_org = org.copy_and_purge_restricted_data()

        assert purged_org is not None
        assert len(purged_org.assemblies) == 1
        assert purged_org.assemblies[0].version == "20"
        assert purged_org.assemblies[0].restricted_to is None
        assert purged_org.assemblies[0].annotations[0].restricted_to is None

        assert purged_org.get_metadata()['restricted_to'] is None
        assert len(purged_org.assemblies) == 1
        assert purged_org.assemblies[0].version == "20"
        assert purged_org.assemblies[0].get_metadata()['restricted_to'] is None
        assert purged_org.assemblies[0].annotations[0].get_metadata()['restricted_to'] is None

        assert purged_org.has_public_data()
        assert not purged_org.has_restricted_data()
        assert not purged_org.has_mixed_data()
        assert purged_org.assemblies[0].has_public_data()
        assert not purged_org.assemblies[0].has_restricted_data()
        assert not purged_org.assemblies[0].has_mixed_data()

        # Did purging altered the original org?
        assert org.restricted_to is None
        assert org.assemblies[0].restricted_to == "some_group"
        assert org.assemblies[0].annotations[0].restricted_to == "some_group"
        assert org.assemblies[1].restricted_to is None
        assert org.assemblies[1].annotations[0].restricted_to is None

        assert org.get_metadata()['restricted_to'] is None
        assert org.assemblies[0].get_metadata()['restricted_to'] == "some_group"
        assert org.assemblies[0].annotations[0].get_metadata()['restricted_to'] == "some_group"
        assert org.assemblies[1].get_metadata()['restricted_to'] is None
        assert org.assemblies[1].annotations[0].get_metadata()['restricted_to'] is None

        assert org.has_public_data()
        assert org.has_restricted_data()
        assert org.has_mixed_data()
        assert org.get_restricted_tos() == ["some_group"]
        assert org.get_restricted_to_map() == {'blabla_blobloblo/assembly_10': 'some_group'}
        assert not org.assemblies[0].has_public_data()
        assert org.assemblies[0].has_restricted_data()
        assert not org.assemblies[0].has_mixed_data()
        assert org.assemblies[0].get_restricted_tos() == ["some_group"]
        assert org.assemblies[0].get_restricted_to_map() == {'blabla_blobloblo/assembly_10': 'some_group'}
        assert org.assemblies[1].has_public_data()
        assert not org.assemblies[1].has_restricted_data()
        assert not org.assemblies[1].has_mixed_data()
        assert org.assemblies[1].get_restricted_tos() == []
        assert org.assemblies[1].get_restricted_to_map() == {}

    def test_multiple_restricted(self, tmp_path_factory):

        with pytest.raises(RuntimeError) as excinfo:
            self.get_org_from_yml_file(tmp_path_factory, "test_multiple_restricted.yml")
        assert str(excinfo.value) == "Multiple different 'restricted_to' rules in a single organism are not supported (found ['another_group', 'some_group'])"

        # If we ever start supporting it some day
        # assert org.get_restricted_tos() == ['some_group', 'another_group']

    def test_restricted_org_derived(self, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        assert list(org.derived_files.keys()) == [
            'build_genoboo',
            'build_elasticsearch',
        ]

        assert org.get_derived_path('build_genoboo').endswith('blabla_blobloblo/build_genoboo/genoboo.tar.bz2')
        assert org.get_derived_path('build_elasticsearch').endswith('blabla_blobloblo/build_elasticsearch/es.tar.bz2')

        assert list(org.assemblies[0].derived_files.keys()) == [
            'jbrowse',
            '2bit',
            'blastdb_nhr',
            'blastdb_nin',
            'blastdb_nog',
            'blastdb_nsd',
            'blastdb_nsi',
            'blastdb_nsq',
        ]

    def test_restricted_ass_derived(self, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        assert list(org.derived_files.keys()) == [
            'build_genoboo',
            'build_genoboo_restricted',
            'build_elasticsearch',
        ]

        assert org.get_derived_path('build_genoboo').endswith('blabla_blobloblo/build_genoboo/genoboo.tar.bz2')
        assert org.get_derived_path('build_elasticsearch').endswith('blabla_blobloblo/build_elasticsearch/es.tar.bz2')

        assert org.get_derived_path('build_genoboo_restricted').endswith('blabla_blobloblo/build_genoboo_restricted/genoboo.tar.bz2')

        assert list(org.assemblies[0].derived_files.keys()) == [
            'jbrowse',
            '2bit',
            'blastdb_nhr',
            'blastdb_nin',
            'blastdb_nog',
            'blastdb_nsd',
            'blastdb_nsi',
            'blastdb_nsq',
        ]

        assert list(org.assemblies[1].derived_files.keys()) == [
            'jbrowse',
            '2bit',
            'blastdb_nhr',
            'blastdb_nin',
            'blastdb_nog',
            'blastdb_nsd',
            'blastdb_nsi',
            'blastdb_nsq',
        ]

        assert org.assemblies[0].get_derived_path('jbrowse').endswith('blabla_blobloblo/assembly_10/jbrowse/jbrowse.tar.gz')

    def test_restricted_ann_derived(self, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        assert list(org.derived_files.keys()) == [
            'build_genoboo',
            'build_genoboo_restricted',
            'build_elasticsearch',
        ]

        assert org.get_derived_path('build_genoboo').endswith('blabla_blobloblo/build_genoboo/genoboo.tar.bz2')
        assert org.get_derived_path('build_elasticsearch').endswith('blabla_blobloblo/build_elasticsearch/es.tar.bz2')

        assert org.get_derived_path('build_genoboo_restricted').endswith('blabla_blobloblo/build_genoboo_restricted/genoboo.tar.bz2')

        assert list(org.assemblies[0].derived_files.keys()) == [
            'jbrowse',
            'jbrowse_restricted',
            '2bit',
            'blastdb_nhr',
            'blastdb_nin',
            'blastdb_nog',
            'blastdb_nsd',
            'blastdb_nsi',
            'blastdb_nsq',
        ]

        assert org.assemblies[0].get_derived_path('jbrowse').endswith('blabla_blobloblo/assembly_10/jbrowse/jbrowse.tar.gz')

        assert org.assemblies[0].get_derived_path('jbrowse_restricted').endswith('blabla_blobloblo/assembly_10/jbrowse_restricted/jbrowse.tar.gz')

        assert list(org.assemblies[1].derived_files.keys()) == [
            'jbrowse',
            '2bit',
            'blastdb_nhr',
            'blastdb_nin',
            'blastdb_nog',
            'blastdb_nsd',
            'blastdb_nsi',
            'blastdb_nsq',
        ]

    def test_unrestricted_org_derived(self, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        assert list(org.derived_files.keys()) == [
            'build_genoboo',
            'build_elasticsearch',
        ]

        assert org.get_derived_path('build_genoboo').endswith('blabla_blobloblo/build_genoboo/genoboo.tar.bz2')
        assert org.get_derived_path('build_elasticsearch').endswith('blabla_blobloblo/build_elasticsearch/es.tar.bz2')

        assert list(org.assemblies[0].derived_files.keys()) == [
            'jbrowse',
            '2bit',
            'blastdb_nhr',
            'blastdb_nin',
            'blastdb_nog',
            'blastdb_nsd',
            'blastdb_nsi',
            'blastdb_nsq',
        ]

        assert org.assemblies[0].get_derived_path('jbrowse').endswith('blabla_blobloblo/assembly_10/jbrowse/jbrowse.tar.gz')

    def test_restricted_org_derived_lock(self, locker, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        self.create_fake_derived_files(org, locker.target_dir)

        org.lock(locker, recursive=True)

        assert org.get_locked_yml()['derived'] == [
            {
                'locked_path': locker.target_dir + '/Blabla/Blobloblo/build_genoboo/0.4.13-unknown-0/genoboo.tar.bz2',
                'name': 'build_genoboo',
                'revision': 0,
                'task_id': 'build_genoboo',
                'tool_version': '0.4.13',
                'type': 'genoboo'
            },
            {
                'locked_path': locker.target_dir + '/Blabla/Blobloblo/build_elasticsearch/8.7.0-unknown-0/es.tar.bz2',
                'name': 'build_elasticsearch',
                'revision': 0,
                'task_id': 'build_elasticsearch',
                'tool_version': '8.7.0',
                'type': 'es'
            }
        ]

    def test_restricted_ass_derived_lock(self, locker, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        self.create_fake_derived_files(org, locker.target_dir)

        org.lock(locker, recursive=True)

        assert org.get_locked_yml()['derived'] == [
            {
                'locked_path': locker.target_dir + '/Blabla/Blobloblo/build_genoboo/0.4.13-unknown-0/genoboo.tar.bz2',
                'name': 'build_genoboo',
                'revision': 0,
                'task_id': 'build_genoboo',
                'tool_version': '0.4.13',
                'type': 'genoboo'
            },
            {
                'locked_path': locker.target_dir + '/Blabla/Blobloblo/build_genoboo_restricted/0.4.13-unknown-0/genoboo.tar.bz2',
                'name': 'build_genoboo_restricted',
                'revision': 0,
                'task_id': 'build_genoboo',
                'tool_version': '0.4.13',
                'type': 'genoboo'
            },
            {
                'locked_path': locker.target_dir + '/Blabla/Blobloblo/build_elasticsearch/8.7.0-unknown-0/es.tar.bz2',
                'name': 'build_elasticsearch',
                'revision': 0,
                'task_id': 'build_elasticsearch',
                'tool_version': '8.7.0',
                'type': 'es'
            },
        ]

    def test_restricted_ann_derived_lock(self, locker, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        self.create_fake_derived_files(org, locker.target_dir)

        org.lock(locker, recursive=True)

        assert org.get_locked_yml()['derived'] == [
            {
                'locked_path': locker.target_dir + '/Blabla/Blobloblo/build_genoboo/0.4.13-unknown-0/genoboo.tar.bz2',
                'name': 'build_genoboo',
                'revision': 0,
                'task_id': 'build_genoboo',
                'tool_version': '0.4.13',
                'type': 'genoboo'
            },
            {
                'locked_path': locker.target_dir + '/Blabla/Blobloblo/build_genoboo_restricted/0.4.13-unknown-0/genoboo.tar.bz2',
                'name': 'build_genoboo_restricted',
                'revision': 0,
                'task_id': 'build_genoboo',
                'tool_version': '0.4.13',
                'type': 'genoboo'
            },
            {
                'locked_path': locker.target_dir + '/Blabla/Blobloblo/build_elasticsearch/8.7.0-unknown-0/es.tar.bz2',
                'name': 'build_elasticsearch',
                'revision': 0,
                'task_id': 'build_elasticsearch',
                'tool_version': '8.7.0',
                'type': 'es'
            },
        ]

    def test_unrestricted_org_derived_lock(self, locker, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        self.create_fake_derived_files(org, locker.target_dir)

        org.lock(locker, recursive=True)

        assert org.get_locked_yml()['derived'] == [
            {
                'locked_path': locker.target_dir + '/Blabla/Blobloblo/build_genoboo/0.4.13-unknown-0/genoboo.tar.bz2',
                'name': 'build_genoboo',
                'revision': 0,
                'task_id': 'build_genoboo',
                'tool_version': '0.4.13',
                'type': 'genoboo'
            },
            {
                'locked_path': locker.target_dir + '/Blabla/Blobloblo/build_elasticsearch/8.7.0-unknown-0/es.tar.bz2',
                'name': 'build_elasticsearch',
                'revision': 0,
                'task_id': 'build_elasticsearch',
                'tool_version': '8.7.0',
                'type': 'es'
            }
        ]
