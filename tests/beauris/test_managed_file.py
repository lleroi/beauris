from beauris.managed_file import DerivedFile, InputFile

from . import BeaurisTestCase


class TestInputFile(BeaurisTestCase):

    def test_from_yml_light(self, fake_file):

        in_yml = {
            "path": fake_file,
            "type": "the type",
        }

        obj = InputFile.from_yml(in_yml, name="blabla", version="21a")

        assert obj.path == in_yml['path']
        assert obj.type == in_yml['type']
        assert obj.version == "21a"
        assert obj.get_revision() == 0
        assert obj.locked_path is None

        assert obj.get_metadata() == {'filename': 'something.txt', 'revision': 0, 'type': 'the type', 'version': '21a'}

        out_yml = {
            "path": fake_file,
            "type": "the type",
            'revision': 0,
        }
        assert obj.to_yml() == out_yml

    def test_from_yml(self, fake_file):

        in_yml = {
            "path": fake_file,
            "type": "the type",
            "revision": 52,
            "locked_path": "/a/locked/path",
        }

        obj = InputFile.from_yml(in_yml, name="blabla", version="big_version")

        assert obj.path == in_yml['path']
        assert obj.type == in_yml['type']
        assert obj.version == "big_version"
        assert obj.get_revision() == in_yml['revision']
        assert obj.locked_path is None

        assert obj.get_metadata() == {'filename': 'something.txt', 'revision': 52, 'type': 'the type', 'version': 'big_version'}

        out_yml = {
            "path": fake_file,
            "type": "the type",
            "revision": 52,
        }

        assert obj.to_yml() == out_yml

    def test_set_metadata(self, fake_file):

        in_yml = {
            "path": fake_file,
            "type": "the type",
        }

        obj = InputFile.from_yml(in_yml, name="blabla", version="2.0")

        obj.metadata = {"foo": "bar", "revision": 12}

        assert obj.path == in_yml['path']
        assert obj.type == in_yml['type']
        assert obj.version == '2.0'
        assert obj.get_revision() == 0
        assert obj.locked_path is None

        assert obj.get_metadata() == {'filename': 'something.txt', 'revision': 12, 'type': 'the type', 'version': '2.0', 'foo': 'bar'}

        out_yml = {
            "path": fake_file,
            "type": "the type",
            "revision": 0,
        }
        assert obj.to_yml() == out_yml

    def test_merge(self, fake_file, another_fake_file):

        in_yml = {
            "path": fake_file,
            "type": "the type",
        }

        obj = InputFile.from_yml(in_yml, name="blabla", version="2.0")

        locked_yml = {
            "locked_path": another_fake_file,
            "type": "the type",
            "revision": 0,
        }

        obj.merge_with_locked(locked_yml)

        assert obj.path == in_yml['path']
        assert obj.type == in_yml['type']
        assert obj.version == "2.0"
        assert obj.get_revision() == 0
        assert obj.locked_path == locked_yml['locked_path']
        assert obj.get_locked_path() == locked_yml['locked_path']

        assert obj.get_metadata() == {'filename': 'something.txt', 'revision': 0, 'type': 'the type', 'version': "2.0"}

        assert obj.to_yml() == {
            "path": fake_file,
            "type": "the type",
            "locked_path": another_fake_file,
            "revision": 0,
        }

    def test_merge_fail(self, fake_file, another_fake_file):

        in_yml = {
            "path": fake_file,
            "type": "the type",
        }

        obj = InputFile.from_yml(in_yml, name="blabla", version="2.0")

        locked_yml = {
            "locked_path": another_fake_file,
            "type": "another type",
        }

        assert obj.is_same_file(locked_yml) is False

        obj.merge_with_locked(locked_yml)

        assert obj.path == in_yml['path']
        assert obj.type == in_yml['type']
        assert obj.version == "2.0"
        assert obj.get_revision() == 0
        assert obj.locked_path is None

        assert obj.get_metadata() == {'filename': 'something.txt', 'revision': 0, 'type': 'the type', 'version': "2.0"}

        assert obj.to_yml() == {
            "path": fake_file,
            "type": "the type",
            "revision": 0,
        }


class TestDerivedFile(BeaurisTestCase):

    def test_from_yml(self, fake_file):

        in_yml = {
            "path": fake_file,
            "type": "the type",
            "tool_version": "big_version",
        }

        obj = DerivedFile.from_yml(in_yml)

        assert obj.locked_path is None
        assert obj.type == in_yml['type']
        assert obj.tool_version == in_yml['tool_version']
        assert obj.get_revision() == 0

        assert obj.get_metadata() == {'filename': 'something.txt', 'revision': 0, 'type': 'the type', 'tool_version': 'big_version', "name": "", 'task_id': ''}

    def test_set_metadata(self, fake_file):

        in_yml = {
            "path": fake_file,
            "name": 'ze_output',
            "type": "the type",
            "tool_version": "2.0",
        }

        obj = DerivedFile.from_yml(in_yml)

        obj.metadata = {"foo": "bar", "revision": 12}

        assert obj.name == in_yml['name']
        assert obj.type == in_yml['type']
        assert obj.get_revision() == 0
        assert obj.locked_path is None
        assert obj.locked_revision is None
        assert obj.tool_version == in_yml["tool_version"]

        assert obj.get_metadata() == {'filename': 'something.txt', 'revision': 12, 'type': 'the type', 'tool_version': "2.0", 'foo': 'bar', 'name': "ze_output", 'task_id': ''}

        out_yml = {
            "locked_path": '',
            "name": 'ze_output',
            "type": "the type",
            "tool_version": "2.0",
            "revision": 0
        }

        assert obj.to_yml() == out_yml

    def test_merge_usable(self, fake_file, another_fake_file):

        in_yml = {
            "path": fake_file,
            "type": "the type",
        }

        obj = DerivedFile.from_yml(in_yml)

        assert obj.get_usable_path() == fake_file

        locked_yml = {
            "locked_path": another_fake_file,
            "type": "the type",
            "revision": 0,
        }

        obj.merge_with_locked(locked_yml)

        assert obj.get_usable_path() == another_fake_file

        assert obj.path == in_yml['path']
        assert obj.type == in_yml['type']
        assert obj.get_revision() == 0
        assert obj.locked_path == locked_yml['locked_path']
        assert obj.get_locked_path() == locked_yml['locked_path']

        assert obj.get_metadata() == {
            'filename': 'something_else.txt',
            'revision': 0,
            'type': 'the type',
            'name': '',
            'task_id': '',
            'tool_version': '',
        }

        assert obj.to_yml() == {
            "type": "the type",
            "locked_path": another_fake_file,
            "revision": 0,
        }
