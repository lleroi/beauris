import os

import yaml

from . import BeaurisTestCase


class TestDeployDownload(BeaurisTestCase):

    def test_interface_data_dir(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_download")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('download', server, org)

        assert deployer._get_data_dir() == os.path.join(deploy_dir, 'Blabla/Blobloblo/docker_data/download')
        assert deployer._get_data_dir("download") == os.path.join(deploy_dir, 'Blabla/Blobloblo/download')

    def test_restricted_org_download(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_download")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('download', server, org)
        out_dir = deployer._get_data_dir("src_data")
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir))
        assert os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_10/fasta/genome.fa"))
        assert os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/gff/test.gff"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/fixed_gff/fixed.gff"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/cds_fa/bblobloblo_ass10_annot1.5_cds.fa"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/proteins_fa/bblobloblo_ass10_annot1.5_proteins.fa"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/transcripts_fa/bblobloblo_ass10_annot1.5_transcripts.fa"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_annot/blast2go.annot"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_gaf/blast2go.gaf"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_pdf/blast2go_report.pdf"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/diamond/diamond_all.xml"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/eggnog/eggnog_annotations.tsv"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/interproscan/interproscan.tsv"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/func_annot_readme/README"))

        out_dir_restricted = deployer._get_data_dir("site", restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "proxy:" in doc
            assert "proxy-restricted:" not in doc

    def test_restricted_ass_download(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_download")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('download', server, org)
        out_dir = deployer._get_data_dir("src_data")
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir))
        assert not os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_10/fasta/genome.fa"))
        assert not os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/gff/test.gff"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/fixed_gff/fixed.gff"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/cds_fa/bblobloblo_ass10_annot1.5_cds.fa"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/proteins_fa/bblobloblo_ass10_annot1.5_proteins.fa"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/transcripts_fa/bblobloblo_ass10_annot1.5_transcripts.fa"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_annot/blast2go.annot"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_gaf/blast2go.gaf"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_pdf/blast2go_report.pdf"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/diamond/diamond_all.xml"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/eggnog/eggnog_annotations.tsv"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/interproscan/interproscan.tsv"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/func_annot_readme/README"))
        assert os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_20/fasta/genome.fa"))
        assert os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/gff/test.gff"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/fixed_gff/fixed.gff"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/cds_fa/bblobloblo_ass20_annot2.5_cds.fa"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/proteins_fa/bblobloblo_ass20_annot2.5_proteins.fa"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/transcripts_fa/bblobloblo_ass20_annot2.5_transcripts.fa"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_annot/blast2go.annot"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_gaf/blast2go.gaf"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_pdf/blast2go_report.pdf"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/diamond/diamond_all.xml"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/eggnog/eggnog_annotations.tsv"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/interproscan/interproscan.tsv"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/func_annot_readme/README"))

        out_dir_restricted = deployer._get_data_dir("src_data", restricted=True)

        assert org.has_mixed_data()
        assert os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/fasta/genome.fa"))
        assert os.path.isfile(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/gff/test.gff"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/fixed_gff/fixed.gff"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/cds_fa/bblobloblo_ass10_annot1.5_cds.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/proteins_fa/bblobloblo_ass10_annot1.5_proteins.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/transcripts_fa/bblobloblo_ass10_annot1.5_transcripts.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_annot/blast2go.annot"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_gaf/blast2go.gaf"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_pdf/blast2go_report.pdf"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/diamond/diamond_all.xml"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/eggnog/eggnog_annotations.tsv"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/interproscan/interproscan.tsv"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/func_annot_readme/README"))
        assert os.path.isfile(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/fasta/genome.fa"))
        assert os.path.isfile(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/gff/test.gff"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/fixed_gff/fixed.gff"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/cds_fa/bblobloblo_ass20_annot2.5_cds.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/proteins_fa/bblobloblo_ass20_annot2.5_proteins.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/transcripts_fa/bblobloblo_ass20_annot2.5_transcripts.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_annot/blast2go.annot"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_gaf/blast2go.gaf"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_pdf/blast2go_report.pdf"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/diamond/diamond_all.xml"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/eggnog/eggnog_annotations.tsv"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/interproscan/interproscan.tsv"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/func_annot_readme/README"))

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = yaml.safe_load(docompf)

            assert "proxy" in doc['services']

            assert "./site/:/usr/local/nginx/html/:ro" in doc['services']['proxy']['volumes']
            assert doc['services']['proxy']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-nginx.rule=(Host(`staging.example.org`) && PathPrefix(`/sp/blabla_blobloblo`))",
                "traefik.http.routers.blabla_blobloblo_staging-nginx.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-nginx.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-nginx.middlewares=sp-auth,sp-trailslash,sp-prefix",
                "traefik.http.services.blabla_blobloblo_staging-nginx.loadbalancer.server.port=80",
            ]

            assert "proxy-restricted" in doc['services']

            assert "./site_restricted/:/usr/local/nginx/html/:ro" in doc['services']['proxy-restricted']['volumes']
            assert doc['services']['proxy-restricted']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-nginx-restricted.rule=(Host(`restricted.staging.example.org`) && PathPrefix(`/sp_restricted/blabla_blobloblo`))",
                "traefik.http.routers.blabla_blobloblo_staging-nginx-restricted.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-nginx-restricted.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-nginx-restricted.middlewares=sp-auth,sp-trailslash,sp-prefix",
                "traefik.http.services.blabla_blobloblo_staging-nginx-restricted.loadbalancer.server.port=80",
            ]

    def test_restricted_ann_download(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_download")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('download', server, org)
        out_dir = deployer._get_data_dir("src_data")
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir))
        assert os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_10/fasta/genome.fa"))
        assert not os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/gff/test.gff"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/fixed_gff/fixed.gff"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/cds_fa/bblobloblo_ass10_annot1.5_cds.fa"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/proteins_fa/bblobloblo_ass10_annot1.5_proteins.fa"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/transcripts_fa/bblobloblo_ass10_annot1.5_transcripts.fa"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_annot/blast2go.annot"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_gaf/blast2go.gaf"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_pdf/blast2go_report.pdf"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/diamond/diamond_all.xml"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/eggnog/eggnog_annotations.tsv"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/interproscan/interproscan.tsv"))
        assert not os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_10/annotation_1.5/func_annot_readme/README"))
        assert os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_20/fasta/genome.fa"))
        assert os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/gff/test.gff"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/fixed_gff/fixed.gff"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/cds_fa/bblobloblo_ass20_annot2.5_cds.fa"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/proteins_fa/bblobloblo_ass20_annot2.5_proteins.fa"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/transcripts_fa/bblobloblo_ass20_annot2.5_transcripts.fa"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_annot/blast2go.annot"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_gaf/blast2go.gaf"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_pdf/blast2go_report.pdf"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/diamond/diamond_all.xml"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/eggnog/eggnog_annotations.tsv"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/interproscan/interproscan.tsv"))
        assert os.path.islink(os.path.join(out_dir, "blabla_blobloblo/assembly_20/annotation_2.5/func_annot_readme/README"))

        out_dir_restricted = deployer._get_data_dir("src_data", restricted=True)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir_restricted))
        assert os.path.isfile(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/fasta/genome.fa"))
        assert os.path.isfile(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/gff/test.gff"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/fixed_gff/fixed.gff"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/cds_fa/bblobloblo_ass10_annot1.5_cds.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/proteins_fa/bblobloblo_ass10_annot1.5_proteins.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/transcripts_fa/bblobloblo_ass10_annot1.5_transcripts.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_annot/blast2go.annot"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_gaf/blast2go.gaf"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/blast2go_pdf/blast2go_report.pdf"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/diamond/diamond_all.xml"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/eggnog/eggnog_annotations.tsv"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/interproscan/interproscan.tsv"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_10/annotation_1.5/func_annot_readme/README"))
        assert os.path.isfile(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/fasta/genome.fa"))
        assert os.path.isfile(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/gff/test.gff"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/fixed_gff/fixed.gff"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/cds_fa/bblobloblo_ass20_annot2.5_cds.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/proteins_fa/bblobloblo_ass20_annot2.5_proteins.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/transcripts_fa/bblobloblo_ass20_annot2.5_transcripts.fa"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_annot/blast2go.annot"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_gaf/blast2go.gaf"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/blast2go_pdf/blast2go_report.pdf"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/diamond/diamond_all.xml"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/eggnog/eggnog_annotations.tsv"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/interproscan/interproscan.tsv"))
        assert os.path.islink(os.path.join(out_dir_restricted, "blabla_blobloblo/assembly_20/annotation_2.5/func_annot_readme/README"))

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "proxy:" in doc
            assert "proxy-restricted:" in doc

    def test_unrestricted_org_download(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_download")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('download', server, org)
        out_dir = deployer._get_data_dir("src_data")
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir))
        assert os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_10/fasta/genome.fa"))

        out_dir_restricted = deployer._get_data_dir("src_data", restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "proxy:" in doc
            assert "proxy-restricted:" not in doc

    def test_download_extra_file(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_extra_files.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_download")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('download', server, org)
        out_dir = deployer._get_data_dir("src_data")
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir))

        assert os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/assembly_10/extra_file_Some_exotic_file/file/es.tar.bz2"))
        assert os.path.isfile(os.path.join(out_dir, "blabla_blobloblo/extra_file_A_big_random_file/file/file.wg"))
