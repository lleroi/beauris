import logging
import os
import random
import string
from pathlib import Path

from beauris import Beauris

import yaml

logging.basicConfig(level=logging.INFO)
log = logging.getLogger()


class BeaurisTestCase():

    def create_fake_input_files(self, entity, locked_dir):

        for id, der in entity.input_files.items():

            if not os.path.exists(der.path):
                Path(der.path).parent.mkdir(parents=True, exist_ok=True)
                # Write some random string to be able to check that locked file is the same
                with open(der.path, 'w') as derf:
                    derf.write(''.join(random.choice(string.ascii_lowercase) for i in range(100)) + '\n')

            if der.locked_path and not os.path.exists(der.locked_path):
                Path(der.locked_path).parent.mkdir(parents=True, exist_ok=True)
                with open(der.locked_path, 'w') as derf:
                    derf.write(''.join(random.choice(string.ascii_lowercase) for i in range(100)) + '\n')

            if der.future_locked_path:
                Path(der.future_locked_path).parent.mkdir(parents=True, exist_ok=True)
                with open(der.future_locked_path, 'w') as derf:
                    derf.write(''.join(random.choice(string.ascii_lowercase) for i in range(100)) + '\n')

        for child in entity.get_children():
            self.create_fake_input_files(child, locked_dir)

    def create_fake_derived_files(self, entity, locked_dir):

        for id, der in entity.derived_files.items():

            Path(der.path).parent.mkdir(parents=True, exist_ok=True)
            # Write some random string to be able to check that locked file is the same
            with open(der.path, 'w') as derf:
                derf.write(''.join(random.choice(string.ascii_lowercase) for i in range(100)) + '\n')

            if der.locked_path:
                Path(der.locked_path).parent.mkdir(parents=True, exist_ok=True)
                with open(der.locked_path, 'w') as derf:
                    derf.write(''.join(random.choice(string.ascii_lowercase) for i in range(100)) + '\n')

            if der.future_locked_path:
                Path(der.future_locked_path).parent.mkdir(parents=True, exist_ok=True)
                with open(der.future_locked_path, 'w') as derf:
                    derf.write(''.join(random.choice(string.ascii_lowercase) for i in range(100)) + '\n')

        for child in entity.get_children():
            self.create_fake_derived_files(child, locked_dir)

    def get_locker(self, work_dir):

        bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))

        locker = bo.get_data_locker(override_conf={'target_dir': str(work_dir)})

        return locker

    def get_org_from_yml(self, tmp_path_factory, yml_data):

        work_dir = tmp_path_factory.mktemp("beauris_tmp")
        yml_path = os.path.join(work_dir, "temp_org.yml")
        with open(yml_path, 'w') as yml_tmp_file:
            yaml.dump(yml_data, yml_tmp_file)

        bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))
        org = bo.load_organism(yml_path, test_data=True)

        return org

    def get_org_from_yml_file(self, tmp_path_factory, yml_name):

        work_dir = tmp_path_factory.mktemp("beauris_tmp")
        bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))
        org = bo.load_organism(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/organisms_chunks/{}'.format(yml_name)), test_data=True)

        return org

    def generate_expected_blast_path(self, output_dir, entity, file_name):

        subfolder = entity.slug(short=True)

        if "transcripts" in file_name:
            subfolder += "_transcripts"
        elif "cds" in file_name:
            subfolder += "_CDS"
        elif 'proteins' in file_name:
            subfolder += "_proteins"

        source_file = entity.get_derived_path(file_name)

        file_name = os.path.splitext(os.path.basename(source_file))[0]
        return os.path.join(output_dir, subfolder, file_name)
