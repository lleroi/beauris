import os

from . import BeaurisTestCase


class TestExtraFilesRestricted(BeaurisTestCase):

    def test_xtra_normal(self, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_extra_files.yml")

        input_root = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data'))

        assert len(org.extra_files) == 1
        assert len(org.assemblies[0].extra_files) == 1

        xtrao = org.extra_files[0]

        assert xtrao.version == "0"
        assert xtrao.name == "A big random file"
        assert xtrao.safe_name == "A_big_random_file"
        assert xtrao.get_input_path("file") == os.path.join(input_root, "data/file.wg")

        xtraa = org.assemblies[0].extra_files[0]

        assert xtraa.version == "0"
        assert xtraa.name == "Some exotic file"
        assert xtraa.safe_name == "Some_exotic_file"
        assert xtraa.get_input_path("file") == os.path.join(input_root, "data/es.tar.bz2")

    def test_xtra_lock(self, locker, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_extra_files.yml")

        input_root = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data'))

        self.create_fake_derived_files(org, locker.target_dir)

        org.lock(locker, recursive=True)

        assert org.get_locked_yml()['extra_files'] == [
            {
                'name': 'A big random file',
                'file': {
                    'locked_path': locker.target_dir + '/Blabla/Blobloblo/A_big_random_file/wig/0/file.wg',
                    'path': os.path.join(input_root, 'data/file.wg'),
                    'revision': 0,
                    'type': 'wig'
                },
            },
        ]

        assert org.assemblies[0].get_locked_yml()['extra_files'] == [
            {
                'name': 'Some exotic file',
                'file': {
                    'locked_path': locker.target_dir + '/Blabla/Blobloblo/10/Some_exotic_file/tar.bz2/0/es.tar.bz2',
                    'path': os.path.join(input_root, 'data/es.tar.bz2'),
                    'revision': 0,
                    'type': 'tar.bz2'
                },
            },
        ]
