import os
import shutil

import yaml

from . import BeaurisTestCase


class TestDeployAuthelia(BeaurisTestCase):

    def test_interface_data_dir(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('authelia', server, org)

        assert deployer._get_data_dir() == os.path.join(deploy_dir, 'Blabla/Blobloblo/docker_data/authelia')
        assert deployer._get_data_dir("authelia") == os.path.join(deploy_dir, 'Blabla/Blobloblo/authelia')

    def test_restricted_org_authelia_empty(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        authelia_conf_dir = tmp_path_factory.mktemp("authelia_conf")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["authelia_conf_merge_with"] = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_other.yml'))

        chunk_path = result_path = os.path.join(authelia_conf_dir, "beauris.yml")
        conf.deploy[server]["options"]["authelia_conf"] = chunk_path
        shutil.copyfile(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_empty.yml')), chunk_path)

        result_path = os.path.join(authelia_conf_dir, "merged.yml")
        conf.deploy[server]["options"]["authelia_conf_merge_to"] = result_path

        deployer = beauris.get_deployer('authelia', server, org)
        deployer.write_data(apply=False)

        with open(chunk_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp/blabla_blobloblo/.*$']
                },
            ]

        with open(result_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'auth.staging.example.org',
                    'policy': 'bypass'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp/blabla_blobloblo/.*$']
                },
                {
                    'domain': 'another.staging.example.org',
                    'policy': 'bypass'
                },
            ]

    def test_restricted_ass_authelia_empty(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        authelia_conf_dir = tmp_path_factory.mktemp("authelia_conf")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["authelia_conf_merge_with"] = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_other.yml'))

        chunk_path = result_path = os.path.join(authelia_conf_dir, "beauris.yml")
        conf.deploy[server]["options"]["authelia_conf"] = chunk_path
        shutil.copyfile(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_empty.yml')), chunk_path)

        result_path = os.path.join(authelia_conf_dir, "merged.yml")
        conf.deploy[server]["options"]["authelia_conf_merge_to"] = result_path

        deployer = beauris.get_deployer('authelia', server, org)
        deployer.write_data(apply=False)

        with open(chunk_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$']
                },
            ]

        with open(result_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'auth.staging.example.org',
                    'policy': 'bypass'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$']
                },
                {
                    'domain': 'another.staging.example.org',
                    'policy': 'bypass'
                },
            ]

    def test_restricted_ann_authelia_empty(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        authelia_conf_dir = tmp_path_factory.mktemp("authelia_conf")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["authelia_conf_merge_with"] = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_other.yml'))

        chunk_path = result_path = os.path.join(authelia_conf_dir, "beauris.yml")
        conf.deploy[server]["options"]["authelia_conf"] = chunk_path
        shutil.copyfile(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_empty.yml')), chunk_path)

        result_path = os.path.join(authelia_conf_dir, "merged.yml")
        conf.deploy[server]["options"]["authelia_conf_merge_to"] = result_path

        deployer = beauris.get_deployer('authelia', server, org)
        deployer.write_data(apply=False)

        with open(chunk_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$']
                },
            ]

        with open(result_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'auth.staging.example.org',
                    'policy': 'bypass'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$']
                },
                {
                    'domain': 'another.staging.example.org',
                    'policy': 'bypass'
                },
            ]

    def test_unrestricted_org_authelia_empty(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        authelia_conf_dir = tmp_path_factory.mktemp("authelia_conf")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["authelia_conf_merge_with"] = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_other.yml'))

        chunk_path = result_path = os.path.join(authelia_conf_dir, "beauris.yml")
        conf.deploy[server]["options"]["authelia_conf"] = chunk_path
        shutil.copyfile(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_empty.yml')), chunk_path)

        result_path = os.path.join(authelia_conf_dir, "merged.yml")
        conf.deploy[server]["options"]["authelia_conf_merge_to"] = result_path

        deployer = beauris.get_deployer('authelia', server, org)
        deployer.write_data(apply=False)

        with open(chunk_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
            ]

        with open(result_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'auth.staging.example.org',
                    'policy': 'bypass'
                },
                {
                    'domain': 'another.staging.example.org',
                    'policy': 'bypass'
                },
            ]

    def test_restricted_org_authelia(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        authelia_conf_dir = tmp_path_factory.mktemp("authelia_conf")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["authelia_conf_merge_with"] = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_other.yml'))

        chunk_path = result_path = os.path.join(authelia_conf_dir, "beauris.yml")
        conf.deploy[server]["options"]["authelia_conf"] = chunk_path
        shutil.copyfile(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_existing.yml')), chunk_path)

        result_path = os.path.join(authelia_conf_dir, "merged.yml")
        conf.deploy[server]["options"]["authelia_conf_merge_to"] = result_path

        deployer = beauris.get_deployer('authelia', server, org)
        deployer.write_data(apply=False)

        with open(chunk_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'foobar.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/foobar/.*$'],
                    'subject': 'group:xx_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp/blabla_blobloblo/.*$']
                },
            ]

        with open(result_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'auth.staging.example.org',
                    'policy': 'bypass'
                },
                {
                    'domain': 'foobar.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/foobar/.*$'],
                    'subject': 'group:xx_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp/blabla_blobloblo/.*$']
                },
                {
                    'domain': 'another.staging.example.org',
                    'policy': 'bypass'
                },
            ]

    def test_restricted_ass_authelia(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        authelia_conf_dir = tmp_path_factory.mktemp("authelia_conf")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["authelia_conf_merge_with"] = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_other.yml'))

        chunk_path = result_path = os.path.join(authelia_conf_dir, "beauris.yml")
        conf.deploy[server]["options"]["authelia_conf"] = chunk_path
        shutil.copyfile(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_existing.yml')), chunk_path)

        result_path = os.path.join(authelia_conf_dir, "merged.yml")
        conf.deploy[server]["options"]["authelia_conf_merge_to"] = result_path

        deployer = beauris.get_deployer('authelia', server, org)
        deployer.write_data(apply=False)

        with open(chunk_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'foobar.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/foobar/.*$'],
                    'subject': 'group:xx_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$']
                },
            ]

        with open(result_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'auth.staging.example.org',
                    'policy': 'bypass'
                },
                {
                    'domain': 'foobar.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/foobar/.*$'],
                    'subject': 'group:xx_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$']
                },
                {
                    'domain': 'another.staging.example.org',
                    'policy': 'bypass'
                },
            ]

    def test_restricted_ann_authelia(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        authelia_conf_dir = tmp_path_factory.mktemp("authelia_conf")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["authelia_conf_merge_with"] = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_other.yml'))

        chunk_path = result_path = os.path.join(authelia_conf_dir, "beauris.yml")
        conf.deploy[server]["options"]["authelia_conf"] = chunk_path
        shutil.copyfile(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_existing.yml')), chunk_path)

        result_path = os.path.join(authelia_conf_dir, "merged.yml")
        conf.deploy[server]["options"]["authelia_conf_merge_to"] = result_path

        deployer = beauris.get_deployer('authelia', server, org)
        deployer.write_data(apply=False)

        with open(chunk_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'foobar.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/foobar/.*$'],
                    'subject': 'group:xx_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$']
                },
            ]

        with open(result_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'auth.staging.example.org',
                    'policy': 'bypass'
                },
                {
                    'domain': 'foobar.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/foobar/.*$'],
                    'subject': 'group:xx_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'restricted.staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp_restricted/blabla_blobloblo/.*$']
                },
                {
                    'domain': 'another.staging.example.org',
                    'policy': 'bypass'
                },
            ]

    def test_unrestricted_org_authelia(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        authelia_conf_dir = tmp_path_factory.mktemp("authelia_conf")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["authelia_conf_merge_with"] = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_other.yml'))

        chunk_path = result_path = os.path.join(authelia_conf_dir, "beauris.yml")
        conf.deploy[server]["options"]["authelia_conf"] = chunk_path
        shutil.copyfile(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_existing.yml')), chunk_path)

        result_path = os.path.join(authelia_conf_dir, "merged.yml")
        conf.deploy[server]["options"]["authelia_conf_merge_to"] = result_path

        deployer = beauris.get_deployer('authelia', server, org)
        deployer.write_data(apply=False)

        with open(chunk_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'foobar.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/foobar/.*$'],
                    'subject': 'group:xx_group'
                },
            ]

        with open(result_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'auth.staging.example.org',
                    'policy': 'bypass'
                },
                {
                    'domain': 'foobar.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/foobar/.*$'],
                    'subject': 'group:xx_group'
                },
                {
                    'domain': 'another.staging.example.org',
                    'policy': 'bypass'
                },
            ]

    def test_restricted_org_same_domain_authelia_empty(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        authelia_conf_dir = tmp_path_factory.mktemp("authelia_conf")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["authelia_conf_merge_with"] = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_other.yml'))

        chunk_path = result_path = os.path.join(authelia_conf_dir, "beauris.yml")
        conf.deploy[server]["options"]["authelia_conf"] = chunk_path
        shutil.copyfile(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_empty.yml')), chunk_path)

        result_path = os.path.join(authelia_conf_dir, "merged.yml")
        conf.deploy[server]["options"]["authelia_conf_merge_to"] = result_path

        # Use the same base url
        conf.deploy[server]["base_url_restricted"] = conf.deploy[server]["base_url"]

        deployer = beauris.get_deployer('authelia', server, org)
        deployer.write_data(apply=False)

        with open(chunk_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp/blabla_blobloblo/.*$']
                },
            ]

        with open(result_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'auth.staging.example.org',
                    'policy': 'bypass'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp/blabla_blobloblo/.*$']
                },
                {
                    'domain': 'another.staging.example.org',
                    'policy': 'bypass'
                },
            ]

    def test_restricted_org_same_domain_authelia(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_authelia")
        authelia_conf_dir = tmp_path_factory.mktemp("authelia_conf")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["authelia_conf_merge_with"] = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_other.yml'))

        chunk_path = result_path = os.path.join(authelia_conf_dir, "beauris.yml")
        conf.deploy[server]["options"]["authelia_conf"] = chunk_path
        shutil.copyfile(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/authelia_existing_same_domain.yml')), chunk_path)

        result_path = os.path.join(authelia_conf_dir, "merged.yml")
        conf.deploy[server]["options"]["authelia_conf_merge_to"] = result_path
        # Use the same base url
        conf.deploy[server]["base_url_restricted"] = conf.deploy[server]["base_url"]

        deployer = beauris.get_deployer('authelia', server, org)
        deployer.write_data(apply=False)

        with open(chunk_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'foobar.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/foobar/.*$'],
                    'subject': 'group:xx_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp/blabla_blobloblo/.*$']
                },
            ]

        with open(result_path, 'r') as autheliaf:
            rules = yaml.safe_load(autheliaf)

            assert "access_control" in rules
            assert "rules" in rules["access_control"]

            assert rules["access_control"]['rules'] == [
                {
                    'domain': 'auth.staging.example.org',
                    'policy': 'bypass'
                },
                {
                    'domain': 'foobar.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/foobar/.*$'],
                    'subject': 'group:xx_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'one_factor',
                    'resources': ['^/sp/blabla_blobloblo/.*$'],
                    'subject': 'group:some_group'
                },
                {
                    'domain': 'staging.example.org',
                    'policy': 'deny',
                    'resources': ['^/sp/blabla_blobloblo/.*$']
                },
                {
                    'domain': 'another.staging.example.org',
                    'policy': 'bypass'
                },
            ]
