import os

import yaml

from . import BeaurisTestCase


class TestDeployJbrowse(BeaurisTestCase):

    def test_interface_data_dir(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_jbrowse")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('jbrowse', server, org)

        assert deployer._get_data_dir() == os.path.join(deploy_dir, 'Blabla/Blobloblo/docker_data/jbrowse')
        assert deployer._get_data_dir("jbrowse") == os.path.join(deploy_dir, 'Blabla/Blobloblo/jbrowse')

    def test_restricted_org_jbrowse(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        org.assemblies[0].derived_files['jbrowse'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/jbrowse.tar.gz'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_jbrowse")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('jbrowse', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        datasets_conf = os.path.join(out_dir, "datasets.conf")

        assert os.path.exists(datasets_conf)
        with open(datasets_conf, 'r') as datl:
            datl = datl.read()
            assert '[datasets.bblobloblo_ass10]\nurl = ?data=data/bblobloblo_ass10\nname = Blabla blobloblo assembly 10\n' in datl

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "jbrowse:" in doc
            assert "jbrowse-restricted:" not in doc

    def test_restricted_ass_jbrowse(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        org.assemblies[0].derived_files['jbrowse'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/jbrowse.tar.gz'))
        org.assemblies[1].derived_files['jbrowse'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/jbrowse.tar.gz'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_jbrowse")
        conf.deploy[server]['target_dir'] = deploy_dir

        # Check deployed files at organism level
        deployer = beauris.get_deployer('jbrowse', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        datasets_conf = os.path.join(out_dir, "datasets.conf")
        assert os.path.exists(datasets_conf)
        with open(datasets_conf, 'r') as datl:
            datl = datl.read()
            assert '[datasets.bblobloblo_ass20]\nurl = ?data=data/bblobloblo_ass20\nname = Blabla blobloblo assembly 20\n' in datl

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert org.has_mixed_data()
        assert os.path.isdir(out_dir_restricted)

        datasets_conf = os.path.join(out_dir_restricted, "datasets.conf")
        assert os.path.exists(datasets_conf)
        with open(datasets_conf, 'r') as datl:
            datl = datl.read()
            assert '[datasets.bblobloblo_ass10]\nurl = ?data=data/bblobloblo_ass10\nname = Blabla blobloblo assembly 10\n' in datl

        # Check deployed files at assembly level
        # This one is restricted, so no folder should exist in public
        assert org.assemblies[0].is_restricted()
        deployer = beauris.get_deployer('jbrowse', server, org.assemblies[0])
        out_dir = os.path.join(deployer._get_data_dir(), org.assemblies[0].slug(short=True))
        out_dir_restricted = os.path.join(deployer._get_data_dir(restricted=True), org.assemblies[0].slug(short=True))
        deployer.write_data()

        assert not os.path.isdir(out_dir)
        assert os.path.isdir(out_dir_restricted)
        tracklist = os.path.join(out_dir_restricted, "trackList.json")
        assert os.path.exists(tracklist)

        with open(tracklist, "r") as tli:
            tl_content = tli.read()
            assert "https://restricted.staging.example.org/sp_restricted/blabla_blobloblo/gnb/gene/{id}?annotation=OGS1.0" in tl_content

        # This one is not restricted, so it should exist both in public and restricted folder
        assert not org.assemblies[1].is_restricted()
        deployer = beauris.get_deployer('jbrowse', server, org.assemblies[1])
        out_dir = os.path.join(deployer._get_data_dir(), org.assemblies[1].slug(short=True))
        out_dir_restricted = os.path.join(deployer._get_data_dir(restricted=True), org.assemblies[1].slug(short=True))
        deployer.write_data()

        assert os.path.isdir(out_dir)
        tracklist = os.path.join(out_dir, "trackList.json")
        assert os.path.exists(tracklist)

        assert os.path.isdir(out_dir_restricted)
        tracklist = os.path.join(out_dir_restricted, "trackList.json")
        assert os.path.exists(tracklist)

        # Check deployed docker-compose file
        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = yaml.safe_load(docompf)

            assert "jbrowse" in doc['services']

            assert "./docker_data/jbrowse/:/jbrowse/data/:ro" in doc['services']['jbrowse']['volumes']
            assert doc['services']['jbrowse']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-jbrowse.rule=(Host(`staging.example.org`) && PathPrefix(`/sp/blabla_blobloblo/jbrowse`))",
                "traefik.http.routers.blabla_blobloblo_staging-jbrowse.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-jbrowse.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-jbrowse.middlewares=sp-auth,sp-app-trailslash,sp-app-prefix",
                "traefik.http.services.blabla_blobloblo_staging-jbrowse.loadbalancer.server.port=80",
            ]

            assert "jbrowse-restricted" in doc['services']

            assert "./docker_data/jbrowse_restricted/:/jbrowse/data/:ro" in doc['services']['jbrowse-restricted']['volumes']
            assert doc['services']['jbrowse-restricted']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-jbrowse-restricted.rule=(Host(`restricted.staging.example.org`) && PathPrefix(`/sp_restricted/blabla_blobloblo/jbrowse`))",
                "traefik.http.routers.blabla_blobloblo_staging-jbrowse-restricted.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-jbrowse-restricted.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-jbrowse-restricted.middlewares=sp-auth,sp-app-trailslash,sp-app-prefix",
                "traefik.http.services.blabla_blobloblo_staging-jbrowse-restricted.loadbalancer.server.port=80",
            ]

    def test_restricted_ann_jbrowse(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        org.assemblies[0].derived_files['jbrowse'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/jbrowse.tar.gz'))
        org.assemblies[0].derived_files['jbrowse_restricted'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/jbrowse.tar.gz'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_jbrowse")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('jbrowse', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        datasets_conf = os.path.join(out_dir, "datasets.conf")
        assert os.path.exists(datasets_conf)
        with open(datasets_conf, 'r') as datl:
            datl = datl.read()
            assert '[datasets.bblobloblo_ass10]\nurl = ?data=data/bblobloblo_ass10\nname = Blabla blobloblo assembly 10\n' in datl

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "jbrowse:" in doc
            assert "jbrowse-restricted:" in doc

    def test_unrestricted_org_jbrowse(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        org.assemblies[0].derived_files['jbrowse'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/jbrowse.tar.gz'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_jbrowse")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('jbrowse', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        datasets_conf = os.path.join(out_dir, "datasets.conf")
        assert os.path.exists(datasets_conf)
        with open(datasets_conf, 'r') as datl:
            datl = datl.read()
            assert '[datasets.bblobloblo_ass10]\nurl = ?data=data/bblobloblo_ass10\nname = Blabla blobloblo assembly 10\n' in datl

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "jbrowse:" in doc
            assert "jbrowse-restricted:" not in doc
