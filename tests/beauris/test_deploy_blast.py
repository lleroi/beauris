import os

import yaml

from . import BeaurisTestCase


class TestDeployblast(BeaurisTestCase):

    def test_task_deploy_blast(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        task_id = 'deploy_blast'

        runner = beauris.get_runner('local', org, task_id, server='staging')
        assert runner.task.get_work_dir() == os.path.join(org.get_work_dir(), "deploy_blast_staging")

        runner = beauris.get_runner('local', org, task_id, server='production')
        assert runner.task.get_work_dir() == os.path.join(org.get_work_dir(), "deploy_blast_production")

        runner = beauris.get_runner('local', org, task_id, server='staging', access_mode="public")
        assert runner.task.get_work_dir() == os.path.join(org.get_work_dir(), "deploy_blast_staging")

        runner = beauris.get_runner('local', org, task_id, server='production', access_mode="public")
        assert runner.task.get_work_dir() == os.path.join(org.get_work_dir(), "deploy_blast_production")

        runner = beauris.get_runner('local', org, task_id, server='staging', access_mode="restricted")
        assert runner.task.get_work_dir() == os.path.join(org.get_work_dir(), "deploy_blast_staging_restricted")

        runner = beauris.get_runner('local', org, task_id, server='production', access_mode="restricted")
        assert runner.task.get_work_dir() == os.path.join(org.get_work_dir(), "deploy_blast_production_restricted")

    def test_interface_data_dir(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_blast")
        blast_job_dir = tmp_path_factory.mktemp("blast_job_dir")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["blast_job_dir"] = blast_job_dir

        deployer = beauris.get_deployer('blast', server, org)

        assert deployer._get_data_dir() == os.path.join(deploy_dir, 'Blabla/Blobloblo/docker_data/blast')
        assert deployer._get_data_dir("blast") == os.path.join(deploy_dir, 'Blabla/Blobloblo/blast')

    def test_restricted_org_blast(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        org.assemblies[0].derived_files['blastdb_nhr'].path = '/blabla_blobloblo/assembly_10/blastdb_assembly/assembly'
        org.assemblies[0].annotations[0].derived_files['blastdb_cds_nhr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_cds/annotation_cds'
        org.assemblies[0].annotations[0].derived_files['blastdb_transcripts_nhr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_transcripts/annotation_transcripts'
        org.assemblies[0].annotations[0].derived_files['blastdb_proteins_phr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_proteins/annotation_proteins'

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_blast")
        blast_job_dir = tmp_path_factory.mktemp("blast_job_dir")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["blast_job_dir"] = blast_job_dir

        deployer = beauris.get_deployer('blast', server, org)
        out_dir = deployer._get_data_dir("blast")
        link_dir = deployer._get_data_dir("blast_files")
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir, "banks.yml"))
        assert os.path.isfile(os.path.join(out_dir, "links.yml"))

        with open(os.path.join(out_dir, "banks.yml")) as banksf:
            banks = yaml.safe_load(banksf)

        assert banks['genouest_blast']['db_provider']['list']['proteic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_proteins_phr'): 'Blabla blobloblo assembly 10 annotation 1.5 proteins'
        }
        assert banks['genouest_blast']['db_provider']['list']['nucleic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[0], 'blastdb_nhr'): 'Blabla blobloblo assembly 10',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_cds_nhr'): 'Blabla blobloblo assembly 10 annotation 1.5 CDS',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_transcripts_nhr'): 'Blabla blobloblo assembly 10 annotation 1.5 transcripts'
        }

        with open(os.path.join(out_dir, "links.yml")) as linksf:
            links = yaml.safe_load(linksf)

        assert links.keys() == {
            'bblobloblo_ass10',
            'bblobloblo_ass10_annot1.5_CDS',
            'bblobloblo_ass10_annot1.5_proteins',
            'bblobloblo_ass10_annot1.5_transcripts',
        }

        out_dir_restricted = deployer._get_data_dir("blast", restricted=True)

        assert not os.path.isdir(out_dir_restricted)
        assert not os.path.isfile(os.path.join(out_dir_restricted, "links.yml"))
        assert not os.path.isfile(os.path.join(out_dir_restricted, "banks.yml"))

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "blast:" in doc
            assert "blast-restricted:" not in doc

    def test_restricted_ass_blast(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        org.assemblies[0].derived_files['blastdb_nhr'].path = '/blabla_blobloblo/assembly_10/blastdb_assembly/assembly'
        org.assemblies[0].annotations[0].derived_files['blastdb_cds_nhr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_cds/annotation_cds'
        org.assemblies[0].annotations[0].derived_files['blastdb_transcripts_nhr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_transcripts/annotation_transcripts'
        org.assemblies[0].annotations[0].derived_files['blastdb_proteins_phr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_proteins/annotation_proteins'
        org.assemblies[1].derived_files['blastdb_nhr'].path = '/blabla_blobloblo/assembly_20/blastdb_assembly/assembly'
        org.assemblies[1].annotations[0].derived_files['blastdb_cds_nhr'].path = '/blabla_blobloblo/assembly_20/annotation_2.5/blastdb_cds/annotation_cds'
        org.assemblies[1].annotations[0].derived_files['blastdb_transcripts_nhr'].path = '/blabla_blobloblo/assembly_20/annotation_2.5/blastdb_transcripts/annotation_transcripts'
        org.assemblies[1].annotations[0].derived_files['blastdb_proteins_phr'].path = '/blabla_blobloblo/assembly_20/annotation_2.5/blastdb_proteins/annotation_proteins'

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_blast")
        conf.deploy[server]['target_dir'] = deploy_dir
        blast_job_dir = tmp_path_factory.mktemp("blast_job_dir")
        conf.deploy[server]["options"]["blast_job_dir"] = blast_job_dir

        deployer = beauris.get_deployer('blast', server, org)
        out_dir = deployer._get_data_dir("blast")
        link_dir = deployer._get_data_dir("blast_files")
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir, "banks.yml"))
        assert os.path.isfile(os.path.join(out_dir, "links.yml"))

        with open(os.path.join(out_dir, "banks.yml")) as banksf:
            banks = yaml.safe_load(banksf)

        assert banks['genouest_blast']['db_provider']['list']['proteic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_proteins_phr'): 'Blabla blobloblo assembly 20 annotation 2.5 proteins',
        }
        assert banks['genouest_blast']['db_provider']['list']['nucleic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[1], 'blastdb_nhr'): 'Blabla blobloblo assembly 20',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_cds_nhr'): 'Blabla blobloblo assembly 20 annotation 2.5 CDS',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_transcripts_nhr'): 'Blabla blobloblo assembly 20 annotation 2.5 transcripts',
        }

        with open(os.path.join(out_dir, "links.yml")) as linksf:
            links = yaml.safe_load(linksf)

        assert links.keys() == {
            'bblobloblo_ass20',
            'bblobloblo_ass20_annot2.5_CDS',
            'bblobloblo_ass20_annot2.5_proteins',
            'bblobloblo_ass20_annot2.5_transcripts',
        }

        out_dir_restricted = deployer._get_data_dir("blast", restricted=True)
        link_dir = deployer._get_data_dir("blast_files", restricted=True)

        assert org.has_mixed_data()
        assert os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir_restricted, "banks.yml"))
        assert os.path.isfile(os.path.join(out_dir_restricted, "links.yml"))

        with open(os.path.join(out_dir_restricted, "banks.yml")) as banksf:
            banks = yaml.safe_load(banksf)

        assert banks['genouest_blast']['db_provider']['list']['proteic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_proteins_phr'): 'Blabla blobloblo assembly 10 annotation 1.5 proteins',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_proteins_phr'): 'Blabla blobloblo assembly 20 annotation 2.5 proteins',
        }
        assert banks['genouest_blast']['db_provider']['list']['nucleic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[0], 'blastdb_nhr'): 'Blabla blobloblo assembly 10',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_cds_nhr'): 'Blabla blobloblo assembly 10 annotation 1.5 CDS',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_transcripts_nhr'): 'Blabla blobloblo assembly 10 annotation 1.5 transcripts',
            self.generate_expected_blast_path(link_dir, org.assemblies[1], 'blastdb_nhr'): 'Blabla blobloblo assembly 20',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_cds_nhr'): 'Blabla blobloblo assembly 20 annotation 2.5 CDS',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_transcripts_nhr'): 'Blabla blobloblo assembly 20 annotation 2.5 transcripts',
        }

        with open(os.path.join(out_dir_restricted, "links.yml")) as linksf:
            links = yaml.safe_load(linksf)

        assert links.keys() == {
            'bblobloblo_ass10',
            'bblobloblo_ass10_annot1.5_CDS',
            'bblobloblo_ass10_annot1.5_proteins',
            'bblobloblo_ass10_annot1.5_transcripts',
            'bblobloblo_ass20',
            'bblobloblo_ass20_annot2.5_CDS',
            'bblobloblo_ass20_annot2.5_proteins',
            'bblobloblo_ass20_annot2.5_transcripts',
        }

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = yaml.safe_load(docompf)

            assert "blast" in doc['services']

            assert './blast/:/etc/blast_links/:ro' in doc['services']['blast']['volumes']
            # No db volume in staging
            # assert './docker_data/blast_db/:/var/lib/postgresql/data/' in doc['services']['blast-db']['volumes']
            assert doc['services']['blast']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-blast.rule=(Host(`staging.example.org`) && PathPrefix(`/sp/blabla_blobloblo/blast`))",
                "traefik.http.routers.blabla_blobloblo_staging-blast.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-blast.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-blast.middlewares=sp-big-req,sp-auth,sp-app-trailslash,sp-app-prefix",
                "traefik.http.services.blabla_blobloblo_staging-blast.loadbalancer.server.port=80",
            ]

            assert "blast-restricted" in doc['services']

            assert './blast_restricted/:/etc/blast_links/:ro' in doc['services']['blast-restricted']['volumes']
            # No db volume in staging
            # assert './docker_data/blast_db_restricted/:/var/lib/postgresql/data/' in doc['services']['blast-db-restricted']['volumes']
            assert doc['services']['blast-restricted']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-blast-restricted.rule=(Host(`restricted.staging.example.org`) && PathPrefix(`/sp_restricted/blabla_blobloblo/blast`))",
                "traefik.http.routers.blabla_blobloblo_staging-blast-restricted.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-blast-restricted.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-blast-restricted.middlewares=sp-big-req,sp-auth,sp-app-trailslash,sp-app-prefix",
                "traefik.http.services.blabla_blobloblo_staging-blast-restricted.loadbalancer.server.port=80",
            ]

    def test_restricted_ann_blast(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        org.assemblies[0].derived_files['blastdb_nhr'].path = '/blabla_blobloblo/assembly_10/blastdb_assembly/assembly'
        org.assemblies[0].annotations[0].derived_files['blastdb_cds_nhr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_cds/annotation_cds'
        org.assemblies[0].annotations[0].derived_files['blastdb_transcripts_nhr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_transcripts/annotation_transcripts'
        org.assemblies[0].annotations[0].derived_files['blastdb_proteins_phr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_proteins/annotation_proteins'
        org.assemblies[0].annotations[1].derived_files['blastdb_cds_nhr'].path = '/blabla_blobloblo/assembly_10/annotation_2.5/blastdb_cds/annotation_cds'
        org.assemblies[0].annotations[1].derived_files['blastdb_transcripts_nhr'].path = '/blabla_blobloblo/assembly_10/annotation_2.5/blastdb_transcripts/annotation_transcripts'
        org.assemblies[0].annotations[1].derived_files['blastdb_proteins_phr'].path = '/blabla_blobloblo/assembly_10/annotation_2.5/blastdb_proteins/annotation_proteins'
        org.assemblies[1].derived_files['blastdb_nhr'].path = '/blabla_blobloblo/assembly_20/blastdb_assembly/assembly'
        org.assemblies[1].annotations[0].derived_files['blastdb_cds_nhr'].path = '/blabla_blobloblo/assembly_20/annotation_2.5/blastdb_cds/annotation_cds'
        org.assemblies[1].annotations[0].derived_files['blastdb_transcripts_nhr'].path = '/blabla_blobloblo/assembly_20/annotation_2.5/blastdb_transcripts/annotation_transcripts'
        org.assemblies[1].annotations[0].derived_files['blastdb_proteins_phr'].path = '/blabla_blobloblo/assembly_20/annotation_2.5/blastdb_proteins/annotation_proteins'

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_blast")
        blast_job_dir = tmp_path_factory.mktemp("blast_job_dir")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["blast_job_dir"] = blast_job_dir

        deployer = beauris.get_deployer('blast', server, org)
        out_dir = deployer._get_data_dir("blast")
        link_dir = deployer._get_data_dir("blast_files")
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir, "banks.yml"))
        assert os.path.isfile(os.path.join(out_dir, "links.yml"))

        with open(os.path.join(out_dir, "banks.yml")) as banksf:
            banks = yaml.safe_load(banksf)

        assert banks['genouest_blast']['db_provider']['list']['proteic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[1], 'blastdb_proteins_phr'): 'Blabla blobloblo assembly 10 annotation 2.5 proteins',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_proteins_phr'): 'Blabla blobloblo assembly 20 annotation 2.5 proteins',
        }
        assert banks['genouest_blast']['db_provider']['list']['nucleic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[0], 'blastdb_nhr'): 'Blabla blobloblo assembly 10',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[1], 'blastdb_cds_nhr'): 'Blabla blobloblo assembly 10 annotation 2.5 CDS',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[1], 'blastdb_transcripts_nhr'): 'Blabla blobloblo assembly 10 annotation 2.5 transcripts',
            self.generate_expected_blast_path(link_dir, org.assemblies[1], 'blastdb_nhr'): 'Blabla blobloblo assembly 20',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_cds_nhr'): 'Blabla blobloblo assembly 20 annotation 2.5 CDS',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_transcripts_nhr'): 'Blabla blobloblo assembly 20 annotation 2.5 transcripts',
        }

        with open(os.path.join(out_dir, "links.yml")) as linksf:
            links = yaml.safe_load(linksf)

        assert links.keys() == {
            'bblobloblo_ass10',
            'bblobloblo_ass10_annot2.5_CDS',
            'bblobloblo_ass10_annot2.5_proteins',
            'bblobloblo_ass10_annot2.5_transcripts',
            'bblobloblo_ass20',
            'bblobloblo_ass20_annot2.5_CDS',
            'bblobloblo_ass20_annot2.5_proteins',
            'bblobloblo_ass20_annot2.5_transcripts',
        }

        out_dir_restricted = deployer._get_data_dir("blast", restricted=True)
        link_dir = deployer._get_data_dir("blast_files", restricted=True)

        assert os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir_restricted, "banks.yml"))
        assert os.path.isfile(os.path.join(out_dir_restricted, "links.yml"))

        with open(os.path.join(out_dir_restricted, "banks.yml")) as banksf:
            banks = yaml.safe_load(banksf)

        assert banks['genouest_blast']['db_provider']['list']['proteic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_proteins_phr'): 'Blabla blobloblo assembly 10 annotation 1.5 proteins',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[1], 'blastdb_proteins_phr'): 'Blabla blobloblo assembly 10 annotation 2.5 proteins',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_proteins_phr'): 'Blabla blobloblo assembly 20 annotation 2.5 proteins',
        }
        assert banks['genouest_blast']['db_provider']['list']['nucleic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[0], 'blastdb_nhr'): 'Blabla blobloblo assembly 10',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_cds_nhr'): 'Blabla blobloblo assembly 10 annotation 1.5 CDS',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_transcripts_nhr'): 'Blabla blobloblo assembly 10 annotation 1.5 transcripts',
            self.generate_expected_blast_path(link_dir, org.assemblies[0], 'blastdb_nhr'): 'Blabla blobloblo assembly 10',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[1], 'blastdb_cds_nhr'): 'Blabla blobloblo assembly 10 annotation 2.5 CDS',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[1], 'blastdb_transcripts_nhr'): 'Blabla blobloblo assembly 10 annotation 2.5 transcripts',
            self.generate_expected_blast_path(link_dir, org.assemblies[1], 'blastdb_nhr'): 'Blabla blobloblo assembly 20',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_cds_nhr'): 'Blabla blobloblo assembly 20 annotation 2.5 CDS',
            self.generate_expected_blast_path(link_dir, org.assemblies[1].annotations[0], 'blastdb_transcripts_nhr'): 'Blabla blobloblo assembly 20 annotation 2.5 transcripts',
        }

        with open(os.path.join(out_dir_restricted, "links.yml")) as linksf:
            links = yaml.safe_load(linksf)

        assert links.keys() == {
            'bblobloblo_ass10',
            'bblobloblo_ass10_annot1.5_CDS',
            'bblobloblo_ass10_annot1.5_proteins',
            'bblobloblo_ass10_annot1.5_transcripts',
            'bblobloblo_ass10',
            'bblobloblo_ass10_annot2.5_CDS',
            'bblobloblo_ass10_annot2.5_proteins',
            'bblobloblo_ass10_annot2.5_transcripts',
            'bblobloblo_ass20',
            'bblobloblo_ass20_annot2.5_CDS',
            'bblobloblo_ass20_annot2.5_proteins',
            'bblobloblo_ass20_annot2.5_transcripts',
        }

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "blast:" in doc
            assert "blast-restricted:" in doc

    def test_unrestricted_org_blast(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        org.assemblies[0].derived_files['blastdb_nhr'].path = '/blabla_blobloblo/assembly_10/blastdb_assembly/assembly'
        org.assemblies[0].annotations[0].derived_files['blastdb_cds_nhr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_cds/annotation_cds'
        org.assemblies[0].annotations[0].derived_files['blastdb_transcripts_nhr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_transcripts/annotation_transcripts'
        org.assemblies[0].annotations[0].derived_files['blastdb_proteins_phr'].path = '/blabla_blobloblo/assembly_10/annotation_1.5/blastdb_proteins/annotation_proteins'

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_blast")
        blast_job_dir = tmp_path_factory.mktemp("blast_job_dir")
        conf.deploy[server]['target_dir'] = deploy_dir
        conf.deploy[server]["options"]["blast_job_dir"] = blast_job_dir

        deployer = beauris.get_deployer('blast', server, org)
        out_dir = deployer._get_data_dir("blast")
        link_dir = deployer._get_data_dir("blast_files")
        deployer.write_data()

        assert os.path.isfile(os.path.join(out_dir, "banks.yml"))
        assert os.path.isfile(os.path.join(out_dir, "links.yml"))

        with open(os.path.join(out_dir, "banks.yml")) as banksf:
            banks = yaml.safe_load(banksf)

        assert banks['genouest_blast']['db_provider']['list']['proteic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_proteins_phr'): 'Blabla blobloblo assembly 10 annotation 1.5 proteins',
        }
        assert banks['genouest_blast']['db_provider']['list']['nucleic'] == {
            self.generate_expected_blast_path(link_dir, org.assemblies[0], 'blastdb_nhr'): 'Blabla blobloblo assembly 10',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_cds_nhr'): 'Blabla blobloblo assembly 10 annotation 1.5 CDS',
            self.generate_expected_blast_path(link_dir, org.assemblies[0].annotations[0], 'blastdb_transcripts_nhr'): 'Blabla blobloblo assembly 10 annotation 1.5 transcripts',
        }

        with open(os.path.join(out_dir, "links.yml")) as linksf:
            links = yaml.safe_load(linksf)

        assert links.keys() == {
            'bblobloblo_ass10',
            'bblobloblo_ass10_annot1.5_CDS',
            'bblobloblo_ass10_annot1.5_proteins',
            'bblobloblo_ass10_annot1.5_transcripts',
        }

        out_dir_restricted = deployer._get_data_dir("blast", restricted=True)

        assert not os.path.isdir(out_dir_restricted)
        assert not os.path.isfile(os.path.join(out_dir_restricted, "banks.yml"))
        assert not os.path.isfile(os.path.join(out_dir_restricted, "links.yml"))

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "blast:" in doc
            assert "blast-restricted:" not in doc
