import os

from . import BeaurisTestCase


class TestExpressionRestricted(BeaurisTestCase):

    def test_exp_normal(self, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_expression.yml")

        assert len(org.assemblies[0].annotations[0].expressions) == 1

        exp = org.assemblies[0].annotations[0].expressions[0]

        assert exp.unit == "XPM"

        assert exp.replicates == [
            {
                'name': 'A first condition',
                'cols': [1, 2, 3],
            },
            {
                'name': 'Another condition',
                'cols': [4, 5, 6],
            },
        ]

    def test_expression_lock(self, locker, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_expression.yml")

        input_root = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data'))

        self.create_fake_derived_files(org, locker.target_dir)

        org.lock(locker, recursive=True)

        assert org.assemblies[0].annotations[0].expressions[0].get_locked_yml()["table"] == {
            'locked_path': locker.target_dir + '/Blabla/Blobloblo/10/1.5/Some_expression_experiment_with_many_conditions/tsv/0/expression.tsv',
            'path': os.path.join(input_root, 'data/expression.tsv'),
            'revision': 0,
            'type': 'tsv'
        }
