import os

import yaml

from . import BeaurisTestCase


class TestDeployGenoboo(BeaurisTestCase):

    def test_interface_data_dir(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genoboo")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genoboo', server, org)

        assert deployer._get_data_dir() == os.path.join(deploy_dir, 'Blabla/Blobloblo/docker_data/genoboo')
        assert deployer._get_data_dir("genoboo") == os.path.join(deploy_dir, 'Blabla/Blobloblo/genoboo')

    def test_restricted_org_genoboo(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_org.yml")

        org.derived_files['build_genoboo'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/genoboo.tar.bz2'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genoboo")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genoboo', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        assert os.path.isdir(os.path.join(out_dir, "mongo_db"))

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "genoboo:" in doc
            assert "genoboo-restricted:" not in doc

    def test_restricted_ass_genoboo(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ass.yml")

        org.derived_files['build_genoboo'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/genoboo.tar.bz2'))
        org.derived_files['build_genoboo_restricted'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/genoboo.tar.bz2'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genoboo")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genoboo', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        gnb_conf = os.path.join(deployer.deploy_base_path, "genoboo.json")
        assert os.path.exists(gnb_conf)
        assert os.path.isdir(os.path.join(out_dir, "mongo_db"))

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert org.has_mixed_data()
        assert os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = yaml.safe_load(docompf)

            assert "genoboo" in doc['services']

            assert doc['services']['genoboo']['volumes'] == [
                "./docker_data/genoboo/mongo_db:/root/db",
                "./genoboo.json:/root/genoboo.json",
            ]
            assert doc['services']['genoboo']['command'] == [
                "genoboo",
                "run",
                "-d",
                "/root/db",
                "-r",
                "https://staging.example.org/sp/blabla_blobloblo/gnb",
                "--config",
                "/root/genoboo.json"
            ]
            assert doc['services']['genoboo']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-genoboo.rule=(Host(`staging.example.org`) && PathPrefix(`/sp/blabla_blobloblo/gnb`))",
                "traefik.http.routers.blabla_blobloblo_staging-genoboo.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-genoboo.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-genoboo.middlewares=sp-auth,sp-app-trailslash",
                "traefik.http.services.blabla_blobloblo_staging-genoboo.loadbalancer.server.port=3000",
            ]

            assert "genoboo-restricted" in doc['services']

            assert doc['services']['genoboo-restricted']['volumes'] == [
                "./docker_data/genoboo_restricted/mongo_db:/root/db",
                "./genoboo_restricted.json:/root/genoboo.json",
            ]
            assert doc['services']['genoboo-restricted']['command'] == [
                "genoboo",
                "run",
                "-d",
                "/root/db",
                "-r",
                "https://restricted.staging.example.org/sp_restricted/blabla_blobloblo/gnb",
                "--config",
                "/root/genoboo.json"
            ]
            assert doc['services']['genoboo-restricted']['deploy']['labels'] == [
                "traefik.http.routers.blabla_blobloblo_staging-genoboo-restricted.rule=(Host(`restricted.staging.example.org`) && PathPrefix(`/sp_restricted/blabla_blobloblo/gnb`))",
                "traefik.http.routers.blabla_blobloblo_staging-genoboo-restricted.tls=true",
                "traefik.http.routers.blabla_blobloblo_staging-genoboo-restricted.entryPoints=webs",
                "traefik.http.routers.blabla_blobloblo_staging-genoboo-restricted.middlewares=sp-auth,sp-app-trailslash",
                "traefik.http.services.blabla_blobloblo_staging-genoboo-restricted.loadbalancer.server.port=3000",
            ]

    def test_restricted_ann_genoboo(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        org.derived_files['build_genoboo'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/genoboo.tar.bz2'))
        org.derived_files['build_genoboo_restricted'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/genoboo.tar.bz2'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genoboo")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genoboo', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        gnb_conf = os.path.join(deployer.deploy_base_path, "genoboo.json")
        assert os.path.exists(gnb_conf)
        assert os.path.isdir(os.path.join(out_dir, "mongo_db"))

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "genoboo:" in doc
            assert "genoboo-restricted:" in doc

    def test_unrestricted_org_genoboo(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")

        org.derived_files['build_genoboo'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/genoboo.tar.bz2'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genoboo")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genoboo', server, org)
        out_dir = deployer._get_data_dir()
        deployer.write_data()

        gnb_conf = os.path.join(deployer.deploy_base_path, "genoboo.json")
        assert os.path.exists(gnb_conf)
        with open(gnb_conf, 'r') as gnbc:
            gnbcr = gnbc.read()
            assert '"disable_user_login": true' in gnbcr
        assert os.path.isdir(os.path.join(out_dir, "mongo_db"))

        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert not os.path.isdir(out_dir_restricted)

        deployer = beauris.get_deployer('dockercompose', server, org)
        deployer.write_data()

        docomp = os.path.join(deployer.deploy_base_path, "docker-compose.yml")
        assert os.path.exists(docomp)
        with open(docomp, 'r') as docompf:
            doc = docompf.read()
            assert "genoboo:" in doc
            assert "genoboo-restricted:" not in doc

    def test_interface_cleanup_unrestricted(self, beauris, tmp_path_factory):
        org = self.get_org_from_yml_file(tmp_path_factory, "test_unrestricted_org.yml")
        org.derived_files['build_genoboo'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/genoboo.tar.bz2'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genoboo")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genoboo', server, org)

        assert deployer.to_cleanup() == []

        deployer.write_data()
        out_dir = deployer._get_data_dir()

        assert deployer.to_cleanup() == [os.path.join(out_dir, "mongo_db_old")]

    def test_interface_cleanup_restricted(self, beauris, tmp_path_factory):

        org = self.get_org_from_yml_file(tmp_path_factory, "test_restricted_ann.yml")

        org.derived_files['build_genoboo'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/genoboo.tar.bz2'))
        org.derived_files['build_genoboo_restricted'].path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/data/genoboo.tar.bz2'))

        server = "staging"
        conf = beauris.config
        deploy_dir = tmp_path_factory.mktemp("deploy_genoboo")
        conf.deploy[server]['target_dir'] = deploy_dir

        deployer = beauris.get_deployer('genoboo', server, org)
        out_dir = deployer._get_data_dir()
        out_dir_restricted = deployer._get_data_dir(restricted=True)

        assert deployer.to_cleanup() == []

        deployer.write_data()

        assert deployer.to_cleanup() == [os.path.join(out_dir, "mongo_db_old"), os.path.join(out_dir_restricted, "mongo_db_old")]
