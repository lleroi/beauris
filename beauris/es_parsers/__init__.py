from .diamond_parser import DiamondParser  # noqa: F401
from .eggnog_parser import EggnogParser  # noqa: F401
from .gff_parser import GffParser  # noqa: F401
from .interpro_parser import InterproParser  # noqa: F401
