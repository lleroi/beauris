import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', 'beauris'))

project = "BEAURIS"

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx_rtd_theme',
    'recommonmark',
]

master_doc = 'index'

html_theme = "sphinx_rtd_theme"


def run_apidoc(_):
    from sphinx.ext.apidoc import main
    parentFolder = os.path.join(os.path.dirname(__file__), '..')
    cur_dir = os.path.abspath(os.path.dirname(__file__))
    sys.path.append(parentFolder)

    module = os.path.join(parentFolder, 'beauris')
    output_path = os.path.join(cur_dir, 'api')
    main(['-e', '-f', '-o', output_path, module])


def setup(app):
    app.connect('builder-inited', run_apidoc)
