BEAURIS - an automated system for the creation of genome portals
================================================================

Contents:

.. toctree::
   :maxdepth: 2

   general
   setting-up
   modules
   api/beauris


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
