# BEAURIS

 [![pipeline status](https://gitlab.com/beaur1s/beauris/badges/main/pipeline.svg)](https://gitlab.com/beaur1s/beauris/-/commits/main) [![PyPI version](https://badge.fury.io/py/beauris.svg)](https://badge.fury.io/py/beauris) [![Documentation Status](https://readthedocs.org/projects/beauris/badge/?version=latest)](https://beauris.readthedocs.io/en/latest/?badge=latest)

BEAURIS: an automated system for the creation of genome portals

Originally written for genomes hosted by [GenOuest](https://www.genouest.org)/GOGEPP on [BIPAA](https://bipaa.genouest.org) and [BBIP](https://bbip.genouest.org), as well as [ABiMS](http://abims.sb-roscoff.fr/) and [SEBIMER](https://bioinfo.ifremer.fr/).

Have a look at the [BEAURIS presentation during JOBIM 2023](https://doi.org/10.5281/zenodo.8279917)

## Documentation

This repository contains the common code needed to deploy any BEAURIS genome portal. To use it, you will need to create another GitLab repository, following the [provided example](https://gitlab.com/beaur1s/sample).

Check out the [BEAURIS documentation](https://beauris.readthedocs.io/).

## Authors

BEAURIS was developped initially by:

- [GenOuest](https://www.genouest.org)/GOGEPP (Rennes, France): used on [BIPAA](https://bipaa.genouest.org) and [BBIP](https://bbip.genouest.org)
- [ABiMS](http://abims.sb-roscoff.fr/) (Roscoff, France)
- [SEBIMER](https://bioinfo.ifremer.fr/) (Brest, France)

See [up-to-date contributors list](https://gitlab.com/beaur1s/beauris/-/graphs/main).
